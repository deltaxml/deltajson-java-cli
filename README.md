# DeltaJSON Commandline JAVA Client
This sample JAVA command-line driver can be used to experiment with DeltaJSON REST API. This command-line driver was written as proof of concept to demonstrate use of the DeltaJSON REST API. 
The source code for this command-line driver is available as a Maven project. 
Please customize as needed. Currently, it uses asynchronous calls with multipart request. Please note that the REST server must be running for this code to operate.

## Running with localhost or user specified host
See [the setup guide](https://docs.deltaxml.com/xml-compare/latest/xml-compare-rest-service/xml-compare-rest-on-premise-installation-and-setup-guide) for instructions.

This client connects to http://localhost:8080 by default, but you can use the property "host" to customize the host, e.g. -Dhost=http://localhost:1234 to point to the REST service running on a different port to the default of 8080.

## Running with our hosted DeltaJSON service (For evaluation purpose)
It is also possible to connect this client to our hosted DeltaJSON service. In order to do this, you need to specify a 'tokenlocation' parameter with all DeltaJSON operations. A value of this paramter is URI to auth0 ID Token JSON file.
To authorise, you will need to use a file containing an auth0 ID Token, auth0 Refresh Token and the expiry for the ID Token.
This file can be downloaded from our [web client](https://www.deltaxml.com/json-client/) after you log in. Simply press "Download ID Token" in the navbar at the top.
This file is read in by the client and the ID token is sent as part of the request to the REST API.
It will use the Refresh Token to get a new ID Token when it expires (and overwrite the value in the file).

## List the Available Commands
Remember the REST server must be running for this code to operate.
```
java -jar target/deltajson-client.jar

 DeltaJSON command-line driver. (C) 2020 DeltaXML Ltd.  All rights reserved.

 Usage:
 java -jar deltajson-client.jar
 java -Dhost=http://localhost:1234 -jar deltajson-client.jar
 Following command describes how to run a specific DeltaJSON operation.
 java -jar deltajson-client.jar describe <operation>
-----------------|---------------------------------------------------------------------------------
 Operation       | Description                                                                     
-----------------|---------------------------------------------------------------------------------
 compare         | Compare two input JSON files and produce a result file.                         
 patch           | Generates a JSON Patch result in RFC6902 JSON Patch format                      
 two-way-merge   | A resolved comparison, where changes are resolved using a merge priority        
 three-way-merge | Three way merge of two JSON files derived from an ancestor file independently.  
 graft           | Applies changes defined in a JSON delta file to any other similar JSON file.    
-----------------|---------------------------------------------------------------------------------   
	
```

## Describe the DeltaJSON operation
This example is for the `compare` operation

```
 java -jar target/deltajson-client.jar describe compare

 Produces a JSON delta file representing changes in two input JSON files.

 java -jar deltajson-client.jar compare <inputAFile> <inputBFile> <resultFile> <parameterName=value>

 Available configuration options are:
=================|===========================|==========================
 Parameter       | Default                   | Available Values         
=================|===========================|==========================
 arrayalignment  | type-with-value-priority  | type-with-value-priority 
                 |                           | position-priority        
                 |                           | orderless                
-----------------|---------------------------|--------------------------
 wordbyword      | false                     | true                     
                 |                           | false                    
-----------------|---------------------------|--------------------------
 changetype      | full-context              | full-context             
                 |                           | changes-only             
-----------------|---------------------------|--------------------------
 dx_config       | URI to a file containing dx_config information                                  
=================|===========================|==========================

 To use our hosted DeltaJSON service, an additional parameter 'tokenlocation' is needed. 
 A 'tokenlocation' specifies URI to a JSON file containing id token for authentication.
 Please refer Readme for instructions about how to obtain an id token JSON file.
```

## Run an operation
```
java -jar target/deltajson-client.jar compare ajson.json bjson.json result.json arrayalignment=position-priority 

 Progress:  
	STARTED
	INPUT_FILTER_CHAIN_A
	INPUT_FILTER_CHAIN_B
	COMPARISON_RUNNING
	OUTPUT_FILTERS
	SAVING
	FINISHED

 Result written to result.json


```

