// Copyright (c) 2024 Deltaman group limited. All rights reserved.
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Authentication {

	private String idtoken = null;

	private String refresh = null;
	private Long expiry;

	private final static String deltaAuthURL = "https://deltaxml.eu.auth0.com/delegation";
	private final static String clientID = "DZzJr0MtT2W1DNZpI8UQqhFWGIndti3A";

	void readDeltaAuth(String tokenlocation) {

		String json = getJSONString(tokenlocation);
		try {
			JSONObject object = (JSONObject) new JSONParser().parse(json);
			idtoken = object.get("id").toString();
			refresh = object.get("refresh").toString();
			expiry = Long.parseLong(object.get("expiry").toString());
		} catch (ParseException e) {
			System.err.println("\n" + e.getMessage() + "\n");
			System.exit(1);
		}
		checkAuth(tokenlocation);
	}

	void checkAuth(String tokenlocation) {
		
		long epoch = System.currentTimeMillis() / 1000;
		if (epoch > expiry) {

			if (refresh == null || refresh.equals("null")) {
				System.out.println("Your ID token has expired and cannot be refreshed."
				+ " Please download a new ID Token file\nhttps://www.deltaxml.com/products/json-compare/docs/rest-guide#authentication");
				System.exit(0);
			} else {
				String json = String.format(
						"{\"client_id\":\"%s\",\n" 
								+ "\"grant_type\":\"urn:ietf:params:oauth:grant-type:jwt-bearer\",\n"
								+ "\"refresh_token\":\"%s\",\n" 
								+ "\"target\":\"%s\",\n"
								+ "\"scope\":\"openid email\",\n" 
								+ "\"api_type\":\"app\"\n" + "}",
						clientID, refresh, clientID);

				URL url;
				try {
					url = new URL(deltaAuthURL);

					HttpURLConnection conn = (HttpURLConnection) url.openConnection();

					conn.setRequestMethod("POST");
					conn.setDoOutput(true);
					conn.setDoInput(true);
					conn.setUseCaches(false);
					conn.setAllowUserInteraction(false);
					conn.setRequestProperty("Content-Type", "application/json");

					BufferedWriter out = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()));
					out.write(json);
					out.close();

					int responseCode = conn.getResponseCode();

					InputStream is;
					if (responseCode != 200 && responseCode != 204) {
						is = conn.getErrorStream();
						System.out.println("Auth0 Error Response:");
						System.out.println(readStream(is));
					} else {
						is = conn.getInputStream();
						String returned = readStream(is);

						JSONObject jsonObject;
						try {
							jsonObject = (JSONObject) new JSONParser().parse(returned);
							idtoken = jsonObject.get("id_token").toString();
							expiry = epoch + Long.parseLong(jsonObject.get("expires_in").toString());
						} catch (ParseException e) {
							System.err.println("\n" + e.getMessage() + "\n");
							System.exit(1);
						}
					}

					String output = String.format("{\"id\": \"%s\" ,"
												+ " \"refresh\": \"%s\" ,"
												+ " \"expiry\": \"%s\"}",
												idtoken, refresh, expiry);

					writeToFile(tokenlocation, output);

				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
	}

	static String getJSONString(String filePath) {
		StringBuilder contentBuilder = new StringBuilder();
		try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
			stream.forEach(s -> contentBuilder.append(s).append("\n"));
		} catch (IOException e) {
			System.err.println("\n" + e.getMessage() + "\n");
			System.exit(1);
		}
		return contentBuilder.toString();
	}
	
	String getAuthHeader() {
		return "Bearer " + idtoken;
	}
	
	String readStream(InputStream i) throws IOException {
        ByteArrayOutputStream result = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int length;

        while ((length = i.read(buffer)) != -1) {
            result.write(buffer, 0, length);
        }
        i.close();

        return result.toString();
    }
	
	void writeToFile(String location, String input) {
        Path toFile = Paths.get(location);

        try {
            Files.write(toFile, input.getBytes());
        } catch (IOException e) {
           System.err.println(e.getMessage());
           System.exit(1);
        }
    }

}
