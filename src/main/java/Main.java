// Copyright (c) 2024 Deltaman group limited. All rights reserved.
import java.io.File;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.MultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * An example of a client using the DeltaJSON REST API from commandline.
 *
 * The REST server must be running for this code to operate.  It connects to localhost for the REST service.
 * The host can be configured using system property 'host'.
 */
public class Main {
	
	public static String operation;
	
	private static int argsLength=0;
	private static int firstParamIndex;
	private static int paramCount;
	private static int resultFileLocation;
	
	static String authHeader="";
	static boolean enableAuthentication=false;
	static String deltaHost="https://www.deltaxml.com";
	static String host="http://0.0.0.0:8080";

	public static void main(String[] args) throws Exception {
		
		if (args.length == 0) {
			PrintUsage.usage();
		} else {
			if (Variables.DESCRIBE.equals(args[0].toLowerCase())) {
				checkArguments(args);
				PrintUsage.describeOperation(operation);
			} else if (isDeltaJSONOperation(args[0].toLowerCase())) {
				checkArguments(args);
				runOperation(args, operation);
			} else {
				System.err.println("\n ERROR: Unknown operation '" + args[0].toLowerCase() + "'");
				PrintUsage.usage();
				System.exit(1);
			}
		}
	}
	
	/**
	 * This function checks the arguments passed from the commandline
	 * 
	 * @param args arguments from the commandline
	 * @throws Exception
	 */
	private static void checkArguments(String[] args) throws Exception {
		
		argsLength = args.length;
		
		if (Variables.DESCRIBE.equals(args[0].toLowerCase())) {
			if (argsLength < 2) {
				System.err.println("\n ERROR: Not enough arguments to " + Variables.DESCRIBE);
				PrintUsage.usage();
				System.exit(1);
			}
			
			operation = args[1].toLowerCase();

			if (!isDeltaJSONOperation(operation)) {
				System.err.println("\n ERROR: Unknown operation '" + operation + "' to " + Variables.DESCRIBE);
				PrintUsage.usage();
				System.exit(1);
			}
		} else {
			operation = args[0].toLowerCase();
			int mandatoryArguments= (Variables.THREE_WAY_MERGE.equals(operation)) ? 5 : 4;
			
			if (argsLength < mandatoryArguments) {
				System.err.println("\n ERROR: Not enough arguments to "+ operation);
				PrintUsage.describeOperation(operation);
				System.exit(1);
			}

			firstParamIndex = getFirstParamIndex(args);
			paramCount = firstParamIndex < 0 ? 0 : (argsLength - firstParamIndex);
			resultFileLocation = firstParamIndex < 0 ? argsLength - 1 : (argsLength - paramCount) - 1;

			int idealResultFileLocation= (Variables.THREE_WAY_MERGE.equals(operation)) ? 4 : 3;
			if (resultFileLocation < idealResultFileLocation) {
				System.err.println("\n ERROR: Not enough arguments or missing result file argument");
				PrintUsage.describeOperation(operation);
				System.exit(1);
			}	
		}
	}
	
	/**
	 * This function runs the JSON operations using POST
	 * 
	 * @param target client info
	 * @param args arguments from the commandline
	 * @throws ParseException 
	 * @throws InterruptedException 
	 * @throws Exception
	 */
	private static void runOperation(String[] args, String op) throws ParseException, InterruptedException {
		
		Client client = ClientBuilder.newBuilder().register(MultiPartFeature.class).build();
		
		MultiPart multiPart = getMultipartEntity(args);
		
		try {
			Response response;
			WebTarget target;
			if (!enableAuthentication) {
				
				if (System.getProperty("host") != null) {
					host = System.getProperty("host");
				}
				
				target = client.target(host + "/api/json/v1");
				
				response = target.path("/" + op)
						 .request()
						 .post(Entity.entity(multiPart, multiPart.getMediaType()));
			} else {
				host=deltaHost;
				System.out.println(authHeader);
				target = client.target(host + "/api/json/v1");
				response = target.path("/" + op)
								 .request()
								 .header("Authorization", authHeader)
								 .post(Entity.entity(multiPart, multiPart.getMediaType()));
			}
	        checkResponse(target, args, response);
		} catch (javax.ws.rs.ProcessingException e) {
			if (e.getCause() instanceof java.net.ConnectException) {
				System.err.println("\n Error: "+ e.getMessage() +". Please check if server is running. \n");
				System.exit(1);
			} else {
				throw e;
			}
		}
	}
	
	/**
	 * This functions returns the multipart entity for a request
	 * 
	 * @param args arguments from the commandline
	 * @return the multipart entity
	 */
	private static MultiPart getMultipartEntity(String[] args) {
		
		List<FormDataBodyPart> formDataBodyPartList= new ArrayList<>();
		
		if (Variables.COMPARE.equals(operation) ||
			Variables.PATCH.equals(operation) ||
			Variables.TWO_WAY_MERGE.equals(operation)) {
			addFormDataBodyPart(formDataBodyPartList, Variables.AJSON, getFileObject(args[1]), MediaType.APPLICATION_JSON_TYPE);
			addFormDataBodyPart(formDataBodyPartList, Variables.BJSON, getFileObject(args[2]), MediaType.APPLICATION_JSON_TYPE);
		} else if (Variables.THREE_WAY_MERGE.equals(operation)) {
			addFormDataBodyPart(formDataBodyPartList, Variables.ANCESTOR, getFileObject(args[1]), MediaType.APPLICATION_JSON_TYPE);
			addFormDataBodyPart(formDataBodyPartList, Variables.EDIT_ONE, getFileObject(args[2]), MediaType.APPLICATION_JSON_TYPE);
			addFormDataBodyPart(formDataBodyPartList, Variables.EDIT_TWO, getFileObject(args[3]), MediaType.APPLICATION_JSON_TYPE);
		} else {
			addFormDataBodyPart(formDataBodyPartList, Variables.CHANGESET, getFileObject(args[1]), MediaType.APPLICATION_JSON_TYPE);
			addFormDataBodyPart(formDataBodyPartList, Variables.TARGET, getFileObject(args[2]), MediaType.APPLICATION_JSON_TYPE);
		}
		
		//adding parameters
		boolean tokenLocationSpecified=false;
		if(paramCount > 0) {
			for (int i=firstParamIndex; i < argsLength; i++) {
				String[] param=args[i].split("=");
				checkParameter(param);
				if (Variables.TOKEN_LOCATION.equals(param[0])) {
					Authentication auth=new Authentication();
					auth.readDeltaAuth(param[1]);
                    authHeader = "Bearer " + auth.getAuthHeader();
                    tokenLocationSpecified=true;
				} else {
					addFormDataBodyPart(formDataBodyPartList, param[0], param[1], MediaType.TEXT_PLAIN_TYPE);
				}
			}
		}
		
		if (tokenLocationSpecified) {
			enableAuthentication=true;
		}
		
		MultiPart multiPart = new MultiPart(MediaType.MULTIPART_FORM_DATA_TYPE);
		
		//asynchronous comparison request
		addFormDataBodyPart(formDataBodyPartList, "async", "true", MediaType.TEXT_PLAIN_TYPE);
		addFormDataBodyPart(formDataBodyPartList, "output", args[resultFileLocation], MediaType.TEXT_PLAIN_TYPE);

		for (FormDataBodyPart formDataBodyPart : formDataBodyPartList) {
			multiPart.bodyPart(formDataBodyPart);
		}
	
		return multiPart;
	}

	/**
	 * @param args
	 * @return
	 */
	public static File getFileObject(String file) {
		File f=new File(file);
		if (!f.exists()) {
			System.err.println("\n ERROR: File (" + file + ") does not exist. \n");
			System.exit(1);
		}
 		return new File(file);
	}

	/**
	 * @param args
	 * @param formDataBodyPartList
	 */
	private static void addFormDataBodyPart(List<FormDataBodyPart> formDataBodyPartList, String name, Object entity, MediaType mediaType) {
		formDataBodyPartList.add(new FormDataBodyPart(name, entity, mediaType));
	}

	/**
	 * @param target
	 * @param args
	 * @param response
	 * @throws ParseException
	 * @throws InterruptedException 
	 */
	private static void checkResponse(WebTarget target, String[] args, Response response) throws ParseException, InterruptedException {
		JSONParser jsonParser = new JSONParser();
        if (response.getStatus() == 202) {
        	//comparison request accepted
        	URI jobURI = response.getLocation();
            String jobSuffix = jobURI.getPath().replace("/api/json/v1/", "");
            boolean finished = false;
            String previousState = "";
            String jobState = null;
            
            System.out.print("\n Progress:  \n");
            
            int maxPollCount=0;
            while (!finished) {
            	if (maxPollCount > 15000) {
            		System.out.print("\n Operation timed out.\n");
            		System.exit(1);
            	}
            	//getting corresponding job for the comparison request
                response =getResponse(jobSuffix, target);
                String jobResponse = response.readEntity(String.class);
                
                JSONObject jobResult = (JSONObject) jsonParser.parse(jobResponse);
                
                //extracting jobStatus from the job response
                jobState = jobResult.get("jobStatus").toString();
                
                finished = "FINISHED".equals(jobState) || "FAILED".equals(jobState);
                if (!previousState.equals(jobState) && !jobState.equals("FAILED")) {
                	System.out.println("	" + jobState);
                    previousState = jobState;
                }
                maxPollCount++;
                Thread.sleep(100);
            }
            
            System.out.println();
            
            if ("FAILED".equals(jobState)) {
                System.out.println(" Comparison failed.");
                
                //getting corresponding job for the comparison request
                response =getResponse(jobSuffix, target);
                String errorResponse = response.readEntity(String.class);
                
                //extracting error from the job response
                JSONObject errorResult = (JSONObject) jsonParser.parse(errorResponse);
                String errorMessage = ((JSONObject) errorResult.get("error")).get("errorMessage").toString();
                String errorCode = ((JSONObject) errorResult.get("error")).get("errorCode").toString();

                System.err.println("\n Comparison failed with error code " + errorCode);
                System.err.printf("\n %-20s\n", "ERROR: " + errorMessage);
                System.exit(1);
            } else {
                System.out.println(" Result written to " + args[resultFileLocation]);
                System.exit(0);
            }
            
        } else {
        	//comparison request failed
        	String errorResponse = response.readEntity(String.class);
            
        	//extracting error from the job response
        	JSONObject errorResult = (JSONObject) jsonParser.parse(errorResponse);
            String errorMessage = errorResult.get("errorMessage").toString();
            
            System.err.println("\n Comparison failed with error code " + response.getStatus());
            System.err.printf("\n %-20s\n", "ERROR: " + errorMessage);	
            PrintUsage.describeOperation(operation);
			System.exit(1);
        }
	}

	/**
	 * This functions validates the parameters passed from commandline
	 * 
	 * @param param a parameter array of size 2 with its name and value
	 * @throws SaxonApiException Exception thrown by Saxon
	 */
	private static void checkParameter(String[] param) {
		if (param.length == 2) {
			if ("".equals(param[0])) {
				 System.err.println("\n ERROR: Parameter name is not present: '" + param[0] + "'");
				 PrintUsage.describeOperation(operation);
				 System.exit(1);
			}
			if ("".equals(param[1])) {
				 System.err.println("\n ERROR: Parameter value is not present: '" + param[1] + "'");
				 PrintUsage.describeOperation(operation);
				 System.exit(1);
			}
		} else {
			System.err.println("\n ERROR: Parameter and its value should be in the form param=value");
			PrintUsage.describeOperation(operation);
			System.exit(1);
		}	
	}
	
	/**
	 * @return
	 */
	private static boolean isDeltaJSONOperation(String op) {
		return Variables.COMPARE.equals(op) ||
			   Variables.PATCH.equals(op) ||
			   Variables.TWO_WAY_MERGE.equals(op) ||
			   Variables.THREE_WAY_MERGE.equals(op) ||
			   Variables.GRAFT.equals(op);
	}

	private static int getFirstParamIndex(String[] args) {
		int result = -1;
		int i = -1;
		for (String s : args) {
			i++;
			if (s.contains("=")) {
				result = i;
				break;
			}
		}
		return result;
	}
	
	/**
    * return HTTP GET method response for a request path 
    * @param path request path
    * @param target client info
    * @return Response
    */
	private static Response getResponse(String path, WebTarget target) {
		Response response;
		response = target.path(path).request().accept(MediaType.APPLICATION_JSON).get();
		return response;
	}
}
