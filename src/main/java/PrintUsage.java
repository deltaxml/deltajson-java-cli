// Copyright (c) 2024 Deltaman group limited. All rights reserved.

public class PrintUsage {
	
	static String usageTableformattedString = " %-15s | %-80s\n";
	static String parameterTableFormattedString = " %-15s | %-25s | %-25s\n";
	
	public static void usage() {

        System.out.println("\n DeltaJSON command-line driver. (C) 2020 DeltaXML Ltd.  All rights reserved.\n");
		System.out.println(" Usage:");
		System.out.println(" java -jar deltajson-client.jar");
		System.out.println(" java -Dhost=http://localhost:1234 -jar deltajson-client.jar");
		System.out.println(" Following command describes how to run a specific DeltaJSON operation.");
		System.out.println(" java -jar deltajson-client.jar describe <operation>");
		System.out.print(String.format(usageTableformattedString, "-", "-", "-", "-").replace(" ", "-"));
		System.out.printf(usageTableformattedString, "Operation", "Description");
		System.out.print(String.format(usageTableformattedString, "-", "-", "-", "-").replace(" ", "-"));
		System.out.printf(usageTableformattedString, Variables.COMPARE, "Compare two input JSON files and produce a result file.");
		System.out.printf(usageTableformattedString, Variables.PATCH, "Generates a JSON Patch result in RFC6902 JSON Patch format");
		System.out.printf(usageTableformattedString, Variables.TWO_WAY_MERGE, "A resolved comparison, where changes are resolved using a merge priority");
		System.out.printf(usageTableformattedString, Variables.THREE_WAY_MERGE, "Three way merge of two JSON files derived from an ancestor file independently.");
		System.out.printf(usageTableformattedString, Variables.GRAFT, "Applies changes defined in a JSON delta file to any other similar JSON file.");
		System.out.print(String.format(usageTableformattedString, "-", "-", "-", "-").replace(" ", "-"));
		System.out.println();
	}
	
	public static void describeOperation(String operation) {
		String opDescription="";
		String inputs="";
		switch (operation) {
		case Variables.COMPARE:
			opDescription="Produces a JSON delta file representing changes in two input JSON files.";
			inputs="<inputAFile> <inputBFile>";
			printInitialOperationInfo(operation, opDescription, inputs);
			startParameterTable();
			arrayAlignmentInfo();
			paramSeparator();
			wordByWordInfo();
			paramSeparator();
			dxConfigInfo();
			tableOutline();
			tokenLocationInfo();
			break;
		case Variables.PATCH:
			opDescription="Generates a JSON Patch result in RFC6902 JSON Patch format for two input JSON files.";
			inputs="<inputAFile> <inputBFile>";
			printInitialOperationInfo(operation, opDescription, inputs);
			startParameterTable();
			patchDirectionInfo();
			paramSeparator();
			dxConfigInfo();
			tableOutline();
			tokenLocationInfo();
			break;
		case Variables.TWO_WAY_MERGE:
			opDescription="Produces a merged JSON file from two JSON inputs by resolving the changes with respect to a specific version.";
			inputs="<inputAFile> <inputBFile>";
			printInitialOperationInfo(operation, opDescription, inputs);
			startParameterTable();
			arrayAlignmentInfo();
			paramSeparator();
			wordByWordInfo();
			paramSeparator();
			mergePriorityInfo();
			paramSeparator();
			dxConfigInfo();
			tableOutline();
			tokenLocationInfo();
			break;
		case Variables.THREE_WAY_MERGE:
			opDescription="Three way merge represents the conflicts or resolves all the changes in two input JSON files derived from an ancestor file independently.";
			inputs="<ancestorFile> <edit1File> <edit2File>";
			printInitialOperationInfo(operation, opDescription, inputs);
			startParameterTable();
			arrayAlignmentInfo();
			paramSeparator();
			wordByWordInfo();
			paramSeparator();
			threeWayMergeModeInfo();
			tableOutline();
			tokenLocationInfo();
			break;
		case Variables.GRAFT:
			opDescription="Graft takes the result of a comparison between two files as a delta (changeSet) and applies the changes to a third related file (target) to update it in a similar way.";
			inputs="<changeset> <target>";
			printInitialOperationInfo(operation, opDescription, inputs);
			startParameterTable();
			graftResolutionModeInfo();
			tableOutline();
			tokenLocationInfo();
			break;
		}
	}

	public static void printInitialOperationInfo(String operation, String opDescription, String inputs) {
		System.out.println("\n " + opDescription);
		System.out.printf("\n java -jar deltajson-client.jar %s %s <resultFile> <parameterName=value>\n", operation, inputs);
		System.out.println("\n Available configuration options are:");
	}
	
	private static void startParameterTable() {
		tableOutline();
		System.out.printf(parameterTableFormattedString, "Parameter", "Default", "Available Values");
		tableOutline();
	}
	
	private static void tableOutline() {
		System.out.print(String.format(parameterTableFormattedString, "=", "=", "=", "=").replace(" ", "="));
	}
	
	private static void paramSeparator() {
		System.out.print(String.format(parameterTableFormattedString, "-", "-", "-", "-").replace(" ", "-"));
	}
	
	private static void graftResolutionModeInfo() {
		System.out.printf(parameterTableFormattedString, Variables.GRAFT_RESOLUTION_MODE, Variables.CHANGESET_PRIORITY, Variables.CHANGESET_PRIORITY);
		System.out.printf(parameterTableFormattedString, "", "", Variables.TARGET_PRIORITY);
		System.out.printf(parameterTableFormattedString, "", "", Variables.ADDTIONS_ONLY);
	}

	private static void threeWayMergeModeInfo() {
		System.out.printf(parameterTableFormattedString, Variables.THREE_WAY_MERGE_MODE, Variables.SHOW_CONFLICTS, Variables.SHOW_CONFLICTS);
		System.out.printf(parameterTableFormattedString, "", "" , Variables.RESOLVE_USING_EDIT1);
		System.out.printf(parameterTableFormattedString, "", "" , Variables.RESOLVE_USING_EDIT2);
	}

	private static void mergePriorityInfo() {
		System.out.printf(parameterTableFormattedString, Variables.MERGE_PRIORITY, Variables.MERGE_PRIORITY_A, Variables.MERGE_PRIORITY_A);
		System.out.printf(parameterTableFormattedString, "", "" , Variables.MERGE_PRIORITY_B);
	}

	private static void dxConfigInfo() {
		System.out.printf(usageTableformattedString, Variables.DX_CONFIG, "URI to a file containing dxConfig information");
	}

	private static void patchDirectionInfo() {
		System.out.printf(parameterTableFormattedString, Variables.PATCH_DIRECTION, Variables.A_TO_B, Variables.A_TO_B);
		System.out.printf(parameterTableFormattedString, "", "" , Variables.B_TO_A);
	}

	private static void wordByWordInfo() {
		System.out.printf(parameterTableFormattedString, Variables.WORD_BY_WORD, Boolean.FALSE, Boolean.TRUE);
		System.out.printf(parameterTableFormattedString, "", "", Boolean.FALSE);
	}

	private static void arrayAlignmentInfo() {
		System.out.printf(parameterTableFormattedString, Variables.ARRAY_ALIGNMENT, Variables.ORDERLESS, Variables.TYPE_WITH_VALUE_PRIORITY);
		System.out.printf(parameterTableFormattedString, "", "", Variables.POSITION_PRIORITY);
		System.out.printf(parameterTableFormattedString, "", "", Variables.ORDERLESS);
	}

	private static void tokenLocationInfo() {
		System.out.println("\n To use our hosted DeltaJSON service, an additional parameter '"+ Variables.TOKEN_LOCATION +"' is needed. ");
		System.out.println(" A '" + Variables.TOKEN_LOCATION + "' specifies URI to a JSON file containing id token for authentication.");
		System.out.println(" Please refer Bitbucket README.md for instructions about how to obtain an id token JSON file.\n");
	}
}
