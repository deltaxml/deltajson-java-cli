// Copyright (c) 2024 Deltaman group limited. All rights reserved.

public class Variables {
	
	public final static String TOKEN_LOCATION = "tokenlocation";
	
	public final static String DESCRIBE = "describe";
	public final static String COMPARE = "compare";
	public final static String PATCH = "patch";
	public final static String TWO_WAY_MERGE = "two-way-merge";
	public final static String THREE_WAY_MERGE = "three-way-merge";
	public final static String GRAFT = "graft";

    public final static String AJSON = "a";
    public final static String BJSON = "b";
    public final static String ANCESTOR = "ancestor";
    public final static String EDIT_ONE = "edit1";
    public final static String EDIT_TWO = "edit2";
    public final static String TARGET = "target";
    public final static String CHANGESET = "changeSet";
    
    public final static String WORD_BY_WORD = "wordByWord";
    
    public final static String DX_CONFIG = "dxConfig";
    
    public final static String ARRAY_ALIGNMENT = "arrayAlignment";
    public final static String TYPE_WITH_VALUE_PRIORITY = "typeWithValuePriority";
    public final static String POSITION_PRIORITY = "positionPriority";
    public final static String ORDERLESS = "orderless";
    
    public final static String PATCH_DIRECTION = "patchDirection";
    public final static String A_TO_B = "aToB";
    public final static String B_TO_A = "bToA";
    
    public final static String MERGE_PRIORITY = "mergePriority";
    public final static String MERGE_PRIORITY_A = "a";
    public final static String MERGE_PRIORITY_B = "b";

    public final static String THREE_WAY_MERGE_MODE = "threeWayMergeMode";
    public final static String RESOLVE_USING_EDIT1 = "resolveUsingEdit1";
    public final static String SHOW_CONFLICTS = "showConflicts";
    public final static String RESOLVE_USING_EDIT2 = "resolveUsingEdit2";
    
    public final static String GRAFT_RESOLUTION_MODE = "graftResolutionMode";
    public final static String CHANGESET_PRIORITY = "changesetPriority";
    public final static String TARGET_PRIORITY = "targetPriority";
    public final static String ADDTIONS_ONLY = "additionsOnly";

}
